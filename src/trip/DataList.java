package trip;

import java.sql.*;
import java.util.ArrayList;

import database.DBOperation;

public class DataList {
	DBOperation dbo=new DBOperation();
	private String[] result;
	private int[] travelCode;
	int resultSize=0;
	
	
	public String[] getResult() {
		return result;
	}
	
	public int[] getTravelCode() {
		return travelCode;
	}

	/**
	 * Search the input place from the column Country ,  travel_code_name
	 * @param place you want to go
	 */
	private void selectTravelCode(String place) {
		String sql = "SELECT travelCode FROM	travel_code \n" 
    			+"WHERE	travelCodeName LIKE '%"+place+"%' "
        		+ "or country LIKE '%"+place+"%';" ;
		dbo.selectData(sql,1);
		result=dbo.getResultSet();
		resultSize=result.length;
		travelCode =new int[resultSize];
		for (int i=0;i<resultSize;i++) {
			travelCode[i]=Integer.valueOf(result[i]);
		}
	}

	/**
	 * Search the input place to find all match trip title 
	 * @param place you want to go
	 * @return match trip title (without duplicated)
	 */
	public String[] searchDestination(String place) {	
		selectTravelCode(place);
		for (int i=0;i<travelCode.length;i++) {
			String sql = "SELECT DISTINCT title FROM trip WHERE travelCode = "+travelCode[i]+";";
			dbo.selectData(sql,1);
		}
		result=dbo.getResultSet();
		resultSize=result.length;
		return result;
	}
	
	


	/**
	 * Search the input place to find all match trip title 
	 * @param place you want to go
	 * @param date is the earliest date you want the trip to start
	 * @return match trip title (without duplicated)
	 */
	public String[] searchDestination(String place,String date) {
		selectTravelCode(place);
		for (int i=0;i<travelCode.length;i++) {
			String sql = "SELECT DISTINCT title FROM trip WHERE travelCode = "+travelCode[i]+" \r\n"
					+ "and date(startDate) >= date('"+date+"');";			
			dbo.selectData(sql,1);
		}
		result=dbo.getResultSet();
		resultSize=result.length;
		return result;
	}
	
	/**
	 * List all the Continent
	 * @return  all the Continent
	*/
	public String[] listContinent() {
		String sql = "SELECT DISTINCT continent FROM travel_code;" ; 
		dbo.selectData(sql, 1);    
		result=dbo.getResultSet();
		resultSize=result.length;
		return result;
	}

	
	/**
	 * List all the country in certain Continent
	 * @param continent is the continent you want to know more
	 * @return those country in certain continent 
	 */
	public String[] listContinentCountry(String continent) {
		String sql = "SELECT DISTINCT country FROM travel_code "
				+ "WHERE continent LIKE '%"+continent+"%' ;";
		dbo.selectData(sql, 1);    
		result=dbo.getResultSet();
		resultSize=result.length;
		return result;
	}
	
	/**
	 * List all the data with certain title
	 * @param title ,the trip which you want to know more
	 * @return match trip detail data
	 */
	public String[] listTitleData(String title) {
		String sql = "SELECT DISTINCT title,price,startDate,endDate FROM trip " 
				+ "WHERE title Like '" + title + "';";
		dbo.selectData(sql, 4);
		result=dbo.getResultSet();
		resultSize=result.length;
		return result;
	}
	
	/**
	 * List all the data with certain title and certain start_date
	 * @param title ,the trip which you want to know more
	 * @param date ,the date you expect the trip start after
	 * @return match trip detail data
	 */
	public String[] listTitleData(String title,String date) {
		String sql = "SELECT DISTINCT title,price,startDate,endDate FROM trip " 
				+ "WHERE title Like '" + title + "' "
				+ "and date(startDate) >= date('"+ date + "');";
		dbo.selectData(sql, 4);
		result=dbo.getResultSet();
		resultSize=result.length;
		return result;
	}
	
	
	
	public String[] listTitleData() {
		String sql = "SELECT DISTINCT title,startDate FROM trip ;";
		dbo.selectData(sql, 2);
		result=dbo.getResultSet();
		resultSize=result.length;
		return result;
	}
	
	
	public static void main(String[] args) {
		DataList dl=new DataList();
		

		for (String j:dl.listTitleData()) {
			System.out.println(j);
		}
		
//		System.out.println("_____");
//		for (String j:dl.searchDestination("中國","2020-08-07")) {
//			System.out.println(j);
//		}
//		System.out.println(dl.resultSize);
//
//		System.out.println("_____");
//
//		for (String j:dl.searchDestination("美國","2020-10-07")) {
//			System.out.println(j);
//		}
//		System.out.println("_____");		
//		for (String j:dl.listContinent()) {
//			System.out.println(j);
//		}
//		System.out.println("_____");		
//		for (String j:dl.listContinentCountry("非洲")) {
//			System.out.println(j);
//		}
//		System.out.println("_____");
//		String s="[春櫻紛飛遊釜慶]世界文化遺產~佛國寺、CNN評選賞櫻推薦~余佐川羅曼史橋+慶和火車站、甘川洞彩繪壁畫村、BIFF廣場+南浦洞購物樂五日<含稅>";
//		for (String j:dl.listTitleData(s)) {
//			System.out.println(j);
//		}
//		System.out.println("_____");
//		for (String j:dl.listTitleData(s,"2020-10-07")) {
//			System.out.println(j);
//		}

	}

}
