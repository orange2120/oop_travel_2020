package trip;

public class Account {

	private String orderDate;
	private String userID;
	private String userPhone;
	private String tripID;
	private int numOfAdult;
	private int numOfChild;
	private int numOfInfant;
	private String title;
	private String travelCode;
	private String productKey;
	private int price;
	private String startDate;
	private String endDate;
	private String lowerBound;
	private String upperBound;
	private int bookedTraveler;
	
	public boolean getAllMems(int orderID) {
		
		
		DBOperation  OP = new DBOperation ();
		OP.selectData("select orderDate, userID, userPhone, tripID, numOfAdult, numOfChild, numOfInfant from OrderData where orderID = " + String.valueOf(orderID), 7);
		
		String[] selectedmems1 = OP.getResultSet();
		
		if (selectedmems1.length > 1) {
			System.out.println("太長了");
		}
		
		if(selectedmems1.length == 0) {
			return false;
		}
		
		else {
			selectedmems1 = selectedmems1[0].split(",");
			
			String orderDate = selectedmems1[0];
			String userID = selectedmems1[1];
			String userPhone = selectedmems1[2];
			String tripID = selectedmems1[3];
			int numOfAdult = Integer.valueOf(selectedmems1[4]);
			int numOfChild = Integer.valueOf(selectedmems1[5]);
			int numOfInfant = Integer.valueOf(selectedmems1[6]);
			
			this.orderDate = orderDate;
			this.userID = userID;
			this.userPhone = userPhone;
			this.tripID = tripID;
			this.numOfAdult = numOfAdult;
			this.numOfChild = numOfChild;
			this.numOfInfant = numOfInfant;
			
			
			
			OP.selectData("select title, travelCode, productKey, price, startDate, endDate, lowerBound, upperBound, bookedTraveler from trip where tripID = " + tripID, 9);
			
			String[] selectedmems2 = OP.getResultSet();
			
			if (selectedmems2.length > 1) {
				System.out.println("太長了");
			}
			
			selectedmems2 = selectedmems2[0].split(",");
			
			String title = selectedmems2[0];
			String travelCode = selectedmems2[1];
			String productKey = selectedmems2[2];
			int price = Integer.valueOf(selectedmems2[3]);
			String startDate = selectedmems2[4];
			String endDate = selectedmems2[5];
			String lowerBound = selectedmems2[6];
			String upperBound = selectedmems2[7];
			int bookedTraveler = Integer.valueOf(selectedmems2[8]);
			
			this.title = title;
			this.travelCode = travelCode;
			this.productKey = productKey;
			this.price = price;
			this.startDate = startDate;
			this.endDate = endDate;
			this.lowerBound = lowerBound;
			this.upperBound = upperBound;
			this.bookedTraveler = bookedTraveler;	
			
			return true;
		}
		
	}
	
	public String getorderDate(int orderID) {
		
		getAllMems(orderID);
		return this.orderDate;		
		
	}
	
	public String getuserID(int orderID) {
		
		getAllMems(orderID);
		return this.userID;		
		
	}
	
	public String getuserPhone(int orderID) {
		
		getAllMems(orderID);
		return this.userPhone;		
		
	}
	
	public String getTripID(int orderID) {
		
		getAllMems(orderID);
		return this.tripID;		
		
	}
	
	public int getNumOfAdult(int orderID) {
		
		getAllMems(orderID);
		
		if(getAllMems(orderID) == false) {
			return -1;
		}
		
		else {
			return this.numOfAdult;
		}
		
	}
	
	public int getNumOfChild(int orderID) {

		getAllMems(orderID);
		
		if(getAllMems(orderID) == false) {
			return -1;
		}
		
		else {
			return this.numOfChild;
		}
		
	}
	
	public int getNumOfInfant(int orderID) {
		
		getAllMems(orderID);
		
		if(getAllMems(orderID) == false) {
			return -1;
		}
		
		else {
			return this.numOfInfant;
		}
		
	}

	public String getTitle(int orderID) {
		
		getAllMems(orderID);
		return this.title;
		
	}
	
	public String getTravelCode(int orderID) {
		
		getAllMems(orderID);
		return this.travelCode;
		
	}
	
	public String getProductKey(int orderID) {
		
		getAllMems(orderID);
		return this.productKey;
		
	}
	
	public int getPrice(int orderID) {
		
		getAllMems(orderID);
		
		if(getAllMems(orderID) == false) {
			return -1;
		}
		
		else {
			return this.price;
		}
		
	}
	
	public String getStartDate(int orderID) {
		
		getAllMems(orderID);
		return this.startDate;
		
	}
	
	public String getEndDate(int orderID) {
		
		getAllMems(orderID);
		return this.endDate;
		
	}
	
	public String getLowerBound(int orderID) {
		
		getAllMems(orderID);
		return this.lowerBound;
		
	}
	
	public String getUpperBound(int orderID) {
		
		getAllMems(orderID);
		return this.upperBound;
		
	}
	
	public int getBookedTraveler(int orderID) {
		
		getAllMems(orderID);
		
		if(getAllMems(orderID) == false) {
			return -1;
		}
		
		else {
			return this.bookedTraveler;
		}
		
	}


}
