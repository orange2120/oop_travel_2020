package trip;

public class UserOperation {
	public String[] bookATrip(int orderID, String userID, String userPhone, String tripID, int numOfAdult, int numOfChild, int numOfInfant) {
		Order order1 = new Order();  
		String returnStmt[] = order1.outputBookImformation(orderID, userID, userPhone, tripID, numOfAdult, numOfChild, numOfInfant);
		return returnStmt;
		//[0]為成功與否，[1]為訊息第一行(若有誤則至此為止)，[2]為訊息第二行，[3]為訊息第三行，[4]為訊息第四行
	}
	
	public String[] deleteTheTrip(int orderID) {
		Order order1 = new Order();	
		String[] returnStmt = order1.outputDeleteImformation(orderID);
		return returnStmt;
		//[0]為成功與否，[1]為訊息第一行
	}
	
	public String[] updateTheTrip(int orderID, int changeNumOfAdult, int changeNumOfChild, int changeNumOfInfant) {
		Order order1 = new Order();	
		String[] returnStmt = order1.outputUpdateImformation(orderID, changeNumOfAdult, changeNumOfChild, changeNumOfInfant);	
		return returnStmt;
		//[0]為成功與否，[1]為訊息第一行
	}

	public String[] inquireTheTrip(int orderID) {
		Order order1 = new Order();	
		String[] returnStmt = order1.outputInquireImformation(orderID);		
		return returnStmt;
		//[0]為成功與否，[1]為訂單編號或錯誤訊息第一行(若有誤則至此為止)，[2]為訊息第一行，[3]為訊息第二行，[4]為訊息第三行，[5]為訊息第四行，[6]為訊息第四行
	}
	
	public String[] inquireOrders(String userID, String userPhone) {
		Order order1 = new Order();	
		String[] returnStmt = order1.outputInquireImformations(userID, userPhone);		
		return returnStmt;
		//[0]為成功與否，[1]為訂單筆數或錯訊息第一行(若有誤則至此為止)，[2][3][4][5][6][7]以後為每筆訂單的orderID、訊息第一行至第五行，依此循環
	}
	
	public String[] searchImf(int orderID) {
		DBOperation  OP = new DBOperation ();
		
		OP.selectData("select orderDate, userID, userPhone, tripID, numOfAdult, numOfChild, numOfInfant from orderData where orderID = " + orderID, 7);
		
		String[] selectedmems = OP.getResultSet();
		
		String[] returnStmt = new String[2];
		
		if (selectedmems.length > 1) {
			//System.out.println("太長了");
			returnStmt[0] = "fail";
			returnStmt[1] = "太長了";
			return returnStmt;
		}
		
		else {
			returnStmt[0] = "fail";
			returnStmt[1] = selectedmems[0];
			return returnStmt;
		}
		
		/*selectedmems = selectedmems[0].split(",");
		
		String orderDate = selectedmems[0];
		String userID = selectedmems[1];
		String userPhone = selectedmems[2];
		String tripID = selectedmems[3];
		int numOfAdult = Integer.valueOf(selectedmems[4]);
		int numOfChild = Integer.valueOf(selectedmems[5]);
		int numOfInfant = Integer.valueOf(selectedmems[6]);
		*/
	}
}
