package trip;

public class TripTestMain {

	public static void main(String[] args) {
		//假如 main function 最初將 int orderID 設為 100000
		int orderID = 100000;
		
		//由 main function 決定要預訂後，接收 User 輸入的 userID、userPhone、numOfAdult、numOfChild、numOfInfant，並取得其選擇行程的 tripID
		String userID1 = "123456";
		String userPhone = "0901-010-101";
		int numOfAdult = 8;
		int numOfChild = 5;
		int numOfInfant = 3;
		String tripID = "2";
		
		//執行 UserOperation UO = new UserOperation(); 並將 orderID++;
		UserOperation UO = new UserOperation();
		orderID++;
		
		//再執行此方法 UO.bookATrip(orderID, userID1, userPhone, tripID, numOfAdult, numOfChild, numOfInfant);
		String[] returnStmt = UO.bookATrip(orderID, userID1, userPhone, tripID, numOfAdult, numOfChild, numOfInfant);	
		if (returnStmt.length != 0) {
			for(int a = 0; a < returnStmt.length; a++) {
				System.out.println(returnStmt[a]);
			}
		}
		
		
		
		
		
		//由 main function 決定要預訂後，接收 User 輸入的 userID、userPhone、numOfAdult、numOfChild、numOfInfant，並取得其選擇行程的 tripID
		String userID10 = "123456";
		String userPhone0 = "0901-010-101";
		int numOfAdult0 = 7;
		int numOfChild0 = 3;
		int numOfInfant0 = 6;
		String tripID0 = "3";
		
		//執行 UserOperation UO = new UserOperation(); 並將 orderID++;
		UserOperation UO0 = new UserOperation();
		orderID++;
		
		//再執行此方法 UO.bookATrip(orderID, userID1, userPhone, tripID, numOfAdult, numOfChild, numOfInfant);
		String[] returnStmt0 = UO0.bookATrip(orderID, userID10, userPhone0, tripID0, numOfAdult0, numOfChild0, numOfInfant0);	
		if (returnStmt0.length != 0) {
			for(int a = 0; a < returnStmt0.length; a++) {
				System.out.println(returnStmt0[a]);
			}
		}
		
		
		
	
		
		
		//由 main function 決定要修改後，接收 User 輸入的 userID、orderID以及三種身分的減少數量(int changeNumOfAdult, int changeNumOfChild, int changeNumOfInfant)
		int ODorderID2 = 100001;
		int changeNumOfAdult = 3;
		int changeNumOfChild = 3;
		int changeNumOfInfant = 3;
		
		//執行 UserOperation UO = new UserOperation();
		UserOperation UO1 = new UserOperation();
		
		//然後再執行此方法 UO.updateTheTrip(orderID, changeNumOfAdult, changeNumOfChild, changeNumOfInfant);
		String[] returnStmt1 = UO1.updateTheTrip(ODorderID2, changeNumOfAdult, changeNumOfChild, changeNumOfInfant);
		if (returnStmt1.length != 0) {
			for(int a = 0; a < returnStmt1.length; a++) {
				System.out.println(returnStmt1[a]);
			}
		}
					
		
		

		
		
		//由 main function 決定要查詢訂單後，接收 User 輸入的 userID、orderID
		int ODorderID3 = 100001;
		
		//執行 UserOperation UO = new UserOperation();
		UserOperation UO11 = new UserOperation();
		
		//然後再執行此方法 UO.inquireTheTrip(orderID);
		String returnStmt11[] = UO11.inquireTheTrip(ODorderID3);
		if (returnStmt11.length != 0) {
			for(int a = 0; a < returnStmt11.length; a++) {
				System.out.println(returnStmt11[a]);
			}
		}
		
		
		
		
		
		//由 main function 決定要查詢訂單後，接收 User 輸入的 userID、userPhone
		String userID44 = "123456";
		String userPhone1 = "0901-010-101";
		
		//執行 UserOperation UO = new UserOperation();
		UserOperation UO111 = new UserOperation();
		
		//然後再執行此方法 UO.inquireTheTrip(orderID);
		String returnStmt111[] = UO111.inquireOrders(userID44, userPhone1);
		if (returnStmt111.length != 0) {
			for(int a = 0; a < returnStmt111.length; a++) {
				System.out.println(returnStmt111[a]);
			}
		}
		
		
		
		
		
		
		//由 main function 決定要退訂後，接收 User 輸入的 userID、orderID
		int ODorderID1 = 100001;
		
		//執行 UserOperation UO = new UserOperation();
		UserOperation UO1111 = new UserOperation();
		
		//然後再執行此方法 UO.deleteTheTrip(orderID);
		String[] returnStmt1111 = UO1111.deleteTheTrip(ODorderID1);
		if (returnStmt1111.length != 0) {
			for(int a = 0; a < returnStmt1111.length; a++) {
				System.out.println(returnStmt1111[a]);
			}
		}
		
		//由 main function 決定要退訂後，接收 User 輸入的 userID、orderID
		int ODorderID10 = 100002;
		
		//執行 UserOperation UO = new UserOperation();
		UserOperation UO11111 = new UserOperation();
		
		//然後再執行此方法 UO.deleteTheTrip(orderID);
		String[] returnStmt11111 = UO11111.deleteTheTrip(ODorderID10);
		if (returnStmt11111.length != 0) {
			for(int a = 0; a < returnStmt11111.length; a++) {
				System.out.println(returnStmt11111[a]);
			}
		}
	}

}
