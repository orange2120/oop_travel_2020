package database;

import java.io.*;
import java.sql.*;

import org.json.simple.*;
import org.json.simple.parser.*;



public class JsonHandle{
	String  projectPath=System.getProperty("user.dir");
	Connection connect = null;
	String json="";
	String[] code;
	public JsonHandle(){
		String str;		
		File jsonFile = new File(projectPath+"/data/travel_code.json");
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(	new FileInputStream(jsonFile),"UTF8"));
			while((str = br.readLine())!=null) {
				json+=str+"\n";
			}
			connect = DriverManager.getConnection("jdbc:sqlite:trip.db");
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public String[] getTravelCode() {
		JSONParser parser=new JSONParser();
		try{
	        Object obj = parser.parse(json);
	        JSONArray array = (JSONArray)obj;
	        code=new String [array.size()];
	        for(int i=0;i<array.size();i++) {
	        	code[i]=(String)((JSONObject)array.get(i)).get("travel_code");
	        }
		}catch(ParseException pe){
	         System.out.println("position: " + pe.getPosition());
	         System.out.println(pe);
	    }return code;
	}
	
	public String getTravelCodeName(int inputCode) {
		JSONParser parser=new JSONParser();
		String output="";
		try{
	        Object obj = parser.parse(json);
	        JSONArray array = (JSONArray)obj;
	        code=new String [array.size()];
	        for(int i=0;i<array.size();i++) {
	        	String travelCode=(String)((JSONObject)array.get(i)).get("travel_code");
	        	int code=Integer.parseInt(travelCode);
	        	if(inputCode==code) {
	        		output=(String)((JSONObject)array.get(i)).get("travel_code_name");
	        	}
	        }
		}catch(ParseException pe){
	         System.out.println("position: " + pe.getPosition());
	         System.out.println(pe);
	    }return output;
	}
	
	public void createTable() {
		try {
			CSVHandle csvH=new CSVHandle();
		    Statement stmt = null;
			stmt = csvH.connect.createStatement();
	        String sql = "CREATE TABLE IF NOT EXISTS travel_code" +
                     "(ID    			   	INT    	 PRIMARY KEY," + 
                     " travel_code 			INT    	 NOT NULL," + 
                     " travel_code_name  	TEXT	 NOT NULL)"; 
			stmt.executeUpdate(sql);			
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}	
	}
	
	public void firstWriteTable() {
		CSVHandle csvH=new CSVHandle();
		csvH.getValidTravelCode();
		JsonHandle jsonH=new JsonHandle();
		String codeInfo="";
		PreparedStatement statement = null;
		Integer id=0;
		try {
			for(String i:jsonH.getTravelCode()) {
				for(int j:csvH.validTravelCode) {
					if (Integer.valueOf(i)==j) {
						codeInfo = "INSERT INTO travel_code ";
						codeInfo += "(ID, travel_code, travel_code_name) ";
						codeInfo += "VALUES (?, ?, ?) ";
						statement = csvH.connect.prepareStatement(codeInfo);
						statement.setString(1,id.toString());
						statement.setString(2,i);
						statement.setString(3,jsonH.getTravelCodeName(j));
						statement.executeUpdate();
						id++;						
					}
				}
			}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void addColunm() {
		try {
			Statement stmt = null;
			stmt = connect.createStatement();			
	        String sql = "ALTER TABLE travel_code "
	        		+ "ADD COLUMN Country text;"
	        		+ "ADD COLUMN Continent text;"; 
			stmt.executeUpdate(sql);		
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void changeColunmName() {
		try {
			Statement stmt = null;
			stmt = connect.createStatement();			
	        String sql = "BEGIN TRANSACTION;"; 
	       
	        sql +=  "CREATE TABLE IF NOT EXISTS travel_code_tep" 
	        		+"(id    			   	INT    	 PRIMARY KEY," 
	        		+" travelCode 			INT    	 NOT NULL," 
	        		+" travelCodeName  		TEXT	 NOT NULL,"
                    +" continent			TEXT	 NOT NULL,"
                    +" country				TEXT	 NOT NULL);\n"; 
	        sql += "INSERT INTO travel_code_tep(id,travelCode,travelCodeName,continent,country) "
                    +" SELECT ID,travel_code,travel_code_name,Continent,Country \r\n" 
	        		+" FROM travel_code;";
	        sql += "DROP TABLE travel_code;";
	        sql += "ALTER TABLE travel_code_tep \r\n" + 
	        		"RENAME TO travel_code;";
	        sql += "COMMIT;";
	        stmt.executeUpdate(sql);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void main(String args[]){ 
		JsonHandle jsh =new JsonHandle();
		jsh.changeColunmName();
		
	}
}
	
