package database;

import java.sql.*;
import java.util.ArrayList;

public class DBOperation {
	Connection connect = null;
	/**
	 * a member used to store selection result
	 */
	ArrayList<String> output =new ArrayList<String>();
	
	
	/**
	 * create the connection with trip.db
	*/
	public DBOperation() {
		try {
			connect = DriverManager.getConnection("jdbc:sqlite:trip.db");
		} catch (SQLException e) {
			// Confirm unsuccessful connection and stop program execution.
			System.out.println("Connection Error !" + e.getMessage());
		}
	}
	
	
	/**
	 * After selection , output the selection result
	 * if there is no data then return array with nothing and print error
	 * @return member output in String[]
	 */
	public String[] getResultSet() {
		String[] resultSet = new String[output.size()];
		for (int i = 0; i < output.size(); i++) {
			resultSet[i] = output.get(i);
		}
		output = new ArrayList<String>();
		if(resultSet.length!=0)	return resultSet;
		else {
			System.out.println("NO DATA!");
			return resultSet;
		}
	}
	

	
	/**
	 * According to input instruction return selecting result ,store the result in member output(ArrayList)
	 * @param sql is the selection want to execute
	 * @param colNum is how many column of data you want (order by sql instruction)
	 */		
	public void selectData(String sql,int colNum) {
		try {
			Statement stmt = connect.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			String row="";
	        while ( rs.next() ) {
	        	for(int i=1;i<=colNum;i++) {
	        		row+=rs.getString(i);
	        		if (i!=colNum) row+=" , ";
	        	}
	        	output.add(row);
	        	row="";
	        }
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	/**
	 * According to input instruction to execute certain action
	 * @param sql is the action execute at database.
	 */
	public void changeDatabase(String sql) {
		try {			
			Statement stmt = connect.createStatement();
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			System.out.println("changeDatabase ERROR "+e.getMessage());
		}
	}
	
	/**
	 * initialize the order table.
	 */
	public void createOrderTable() {
		try {
			Statement stmt = connect.createStatement();
			String sql = "CREATE TABLE IF NOT EXISTS OrderData" 
					+ "(orderID		PRIMARY KEY,"
					+ " userID		TEXT	NOT NULL," 
					+ " userPhone	INT		NOT NULL,"
					+ " tripID      INT    	NOT NULL,"
					+ " numOfAdult  INT    	NOT NULL," 
					+ " numOfChild  INT		NOT NULL,"
					+ " numOfInfant INT		NOT NULL)";
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] args) {
		 DBOperation dbo=new DBOperation();
//		 String sql = "SELECT DISTINCT * FROM travel_code WHERE ID = 1;" ;	
//		 dbo.selectData(sql,5);	 
//		 for (String s:dbo.getResultSet()) {
//			 System.out.println(s);
//		 }
	//	 System.out.println(dbo.getResultSet()[1]);
		 dbo.createOrderTable();

	}
}
