package database;

import java.io.File;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class CSVHandle {
	String projectPath=System.getProperty("user.dir");
	File csvFile=new File(projectPath+"/data/trip_data_all.csv");
    Connection connect = null;
    Reader reader;
    Iterable<CSVRecord> records;
    int[] validTravelCode=null;
    
	public CSVHandle() {
    	// CSV file path.
         if (!csvFile.isFile()) {
            // Confirm incorrect CSV file location and stop program execution.
            System.out.println("Error locating CSV file.");
            System.exit(0);
        }
        try {
            // Connect to database.
            connect = DriverManager.getConnection("jdbc:sqlite:trip.db");
            System.out.println("Database connection successful.");
        } catch (SQLException e) {
            // Confirm unsuccessful connection and stop program execution.
            System.out.println(e.getMessage());
        }		        
	}

	public void createTable() {
		try {
		    Statement stmt = null;
			stmt = connect.createStatement();
	        String sql = "CREATE TABLE IF NOT EXISTS trip" +
                     "(tripID INTEGER PRIMARY KEY ,"+
	        		 " title       	TEXT    NOT NULL," + 
                     " travelCode  INT    	NOT NULL," + 
                     " price       INT		NOT NULL,"+
                     " startDate   TEXT		NOT NULL,"+
                     " endDate     TEXT		NOT NULL,"+
                     " lowerBound  INT		NOT NULL,"+
                     " upperBound  INT		NOT NULL)"; 
			stmt.executeUpdate(sql);			
		}catch(Exception e) {
			System.out.println("createTable error "+e.getMessage());
		}	
	}

	public void csvToDb() {
		try {
			 // Assign CSV file to reader object and extract the records.
	         Reader reader = Files.newBufferedReader(Paths.get(csvFile.getPath()));
	         records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(reader);
	         String sqlTripInfo=null;
	         PreparedStatement statement = null;
	//         int i=1;
			 for (CSVRecord record:records) {
				 sqlTripInfo = "INSERT INTO trip";
				 sqlTripInfo += "(title, travelCode, price, startDate, endDate, lowerBound, upperBound)";
				 sqlTripInfo += "VALUES (?, ?, ?, ?, ?, ?, ?) ";
				 statement = connect.prepareStatement(sqlTripInfo);
				 statement.setString(1, record.get("title"));
				 statement.setString(2, record.get("travel_code"));
				 statement.setString(3, record.get("price"));
				 statement.setString(4, record.get("start_date"));
				 statement.setString(5, record.get("end_date"));
				 statement.setString(6, record.get("lower_bound"));
				 statement.setString(7, record.get("upper_bound"));
				 statement.executeUpdate();
			 }
			 statement.close();
			 connect.commit();
			 connect.close();
		}catch (Exception e) {
	         System.out.println("csvToDb error " +e.getMessage());
		}
	}

	public void getValidTravelCode() {
		try {
			Statement stmt = null;
			stmt = connect.createStatement();
	        String travelCode = "SELECT DISTINCT travel_code FROM trip;"; 
	        ResultSet rs = stmt.executeQuery(travelCode);
	        validTravelCode=new int [50];
	        int i=0;
	        while ( rs.next() ) {
	            int TravelCode = rs.getInt("travel_code");
	            validTravelCode[i] = TravelCode;
	            i++;
	        }
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	/**
	 * check each trip data upperbound>lowerbound, if not ,exchange the data of these two column 
	 */
	public void adjustData() {
		try {
			Statement stmt = null;
			stmt = connect.createStatement();
	        String sql = "SELECT tripID,upperBound,lowerBound FROM trip;"; 
	        ResultSet rs = stmt.executeQuery(sql);
	        ArrayList<Integer> id =new ArrayList<Integer>();
	        sql="";
	        while ( rs.next() ) {
	            int ub=rs.getInt("upperBound");
	            int lb=rs.getInt("lowerBound");
	            if(lb>ub) {
	            	id.add(rs.getInt("tripID"));	            	
	            	sql+="UPDATE trip SET upperBound ="+lb+", lowerBound ="+ub+" WHERE tripID ="+rs.getInt("tripID")+";";        	
	            }
	        }
	        stmt.executeUpdate(sql);
	        System.out.println(id);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

	

	
	public static void main(String[] args) {
		CSVHandle csvH=new CSVHandle();
		//initialize data
		csvH.createTable();
		csvH.csvToDb();
		//adjust table		
		csvH.adjustData();

	}

}
