package com.example.oop_travel_app.develop_related;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.oop_travel_app.R;

public class DevelopHomepage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_develop_homepage);
    }
}
