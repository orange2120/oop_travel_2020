package com.example.oop_travel_app.account_related;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.oop_travel_app.R;

public class AccountHomepage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_homepage);
    }
}
